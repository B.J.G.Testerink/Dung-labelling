package org.andortheory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;
		
public final class AndOrGraph { 
	@SuppressWarnings("unchecked")
	public final static <P> Entry<Argument<P>[], Function<Argument<P>, Set<Argument<P>>>> produceDungGraph(final Map<P, OrNode<P>> andOrGraph, final Set<P> observations, 
			final BiPredicate<P, P> contrariness){
		// Get the arguments
		final Map<P, Set<Argument<P>>> conclusionToArguments = makeArguments(andOrGraph, observations); 
		final List<Argument<P>> arguments = new ArrayList<>();
		for(P proposition : conclusionToArguments.keySet())
			arguments.addAll(conclusionToArguments.get(proposition));
		
		// Make the attack relation
		final Map<Argument<P>, Set<Argument<P>>> attackRelation = new HashMap<>();
		for(Argument<P> attacker : arguments) {
			for(Argument<P> attacked : arguments) {
				for(P dependency : attacked.getPropositionDependencies()) {
					if(contrariness.test(attacker.getConclusion(), dependency)) {
						attackRelation.computeIfAbsent(attacker, a -> new HashSet<>()).add(attacked); 
						break;
					}
				}
			}
		}

		return new SimpleEntry<>(
				(Argument<P>[]) arguments.toArray(new Argument[arguments.size()]), 
				a -> attackRelation.get(a));
	}
	
	private final static <P> Map<P, Set<Argument<P>>> makeArguments(final Map<P, OrNode<P>> graph, final Set<P> observations) {
		// Activate the children
		for(P observation : observations)
			graph.get(observation).observed();
		
		final Map<P, Set<Argument<P>>> conclusionToArguments = new HashMap<>();
		
		// Activate the observations and get the and-nodes that possibly generate new arguments
		Set<AndNode<P>> nextRound = new HashSet<>();
		for(P observation : observations) { 
			conclusionToArguments.computeIfAbsent(observation, a -> new HashSet<>()).add(new Argument<P>(observation));
			for(AndNode<P> parent : graph.get(observation).getParents())
				if(isActive(parent, conclusionToArguments))
					nextRound.add(parent);
		}
		
		// Iterative make arguments (inference trees) from increasing height (where max height is the number of or-nodes)
		Set<AndNode<P>> front = new HashSet<>();
		while(!nextRound.isEmpty()) {
			front = nextRound;
			nextRound = new HashSet<>();
			for(AndNode<P> node : front) {
				// Every front node might have new arguments that it can generate
				final OrNode<P> conclusion = node.getParent();
				// Make arguments
				final int nrOfCurrentArguments = nrOfArguments(conclusionToArguments, conclusion.getContent());
				findNewArgumentsFor(node, conclusionToArguments);
				final int nrOfNewArguments = nrOfArguments(conclusionToArguments, conclusion.getContent()) - nrOfCurrentArguments;
				// If the node generates new arguments, then its parents may generate new arguments
				if(nrOfNewArguments > 0) {
					for(AndNode<P> next : conclusion.getParents()) {
						// Only add the parents if for each of its premises there exists an argument 
						if(isActive(next, conclusionToArguments))
							nextRound.add(next);
					} 
				}
			} 
		}
		return conclusionToArguments;
	}
	
	private final static <P> boolean isActive(final AndNode<P> node, final Map<P, Set<Argument<P>>> conclusionToArguments) {
		for(OrNode<P> premise : node.getChildren())
			if(conclusionToArguments.get(premise.getContent()) == null)
				return false;
		return true;
	}
	
	private final static <P> int nrOfArguments(final Map<P, Set<Argument<P>>> conclusionToArguments, final P conclusion) {
		return conclusionToArguments.getOrDefault(conclusion, Collections.emptySet()).size();
	}
	
	private final static <P> void findNewArgumentsFor(final AndNode<P> rule, final Map<P, Set<Argument<P>>> conclusionToArguments) {
		// Collect the arguments for each premise (becomes a vector of argument sets of which each combination 
		// constitutes an argument)
		final List<Set<Argument<P>>> premiseOptions = new ArrayList<>();
		for(OrNode<P> child : rule.getChildren())
			premiseOptions.add(conclusionToArguments.get(child.getContent()));  
		visitCombinations(
				rule.getParent().getContent(), 
				premiseOptions, 
				0, 
				new ArrayList<>(), 
				conclusionToArguments.computeIfAbsent(rule.getParent().getContent(), p -> new HashSet<>()));
	}
	
	/** Visit all combinations of arguments in the premises (each of which constitutes an argument) and check whether 
	 * the conclusion, given those premises is a new and legal argument. */
	private final static <P> void visitCombinations(final P conclusion, final List<Set<Argument<P>>> premiseOptions, final int depth, final List<Argument<P>> current, final Set<Argument<P>> existing) {
		if(depth == premiseOptions.size()) {
			// Check if the argument is new
			for(Argument<P> oldArgument : existing) 
				if(oldArgument.getPremises().containsAll(current))
					return;
			// Check if argument is legal (no double occurrences of premises on a path)
			//final Set<P> taboo = new HashSet<>();
			//taboo.add(conclusion);
			if(legal(conclusion, current)) {
				final Argument<P> newArgument = new Argument<P>(conclusion);
				for(Argument<P> premise : current)
					newArgument.addPremise(premise);
				existing.add(newArgument);
			}
		} else {   
			// Go through all the options of this premise
			for(Argument<P> argument : premiseOptions.get(depth)) {
				current.add(argument);
				visitCombinations(conclusion, premiseOptions, depth + 1, current, existing);
				current.remove(current.size() - 1);
			}
		}
	} 
	
	private final static <P> boolean legal(final P taboo, final List<Argument<P>> premises) {
		for(Argument<P> premise : premises)
			if(premise.getPropositionDependencies().contains(taboo))
				return false;
		return true; 
	} 
	
	public final static <P> Map<P, OrNode<P>> buildAndOrGraph(final Map<P, Set<Set<P>>> rules){
		// Build the and/or graph
		final Map<P, OrNode<P>> propositionToNode = new HashMap<>();
		for(P proposition : rules.keySet()) {
			final OrNode<P> conclusionNode = getNode(proposition, propositionToNode);
			final Set<Set<P>> bodies = rules.getOrDefault(proposition, Collections.emptySet());
			for(Set<P> body : bodies) {
				final AndNode<P> ruleNode = new AndNode<>(conclusionNode); 
				conclusionNode.addChild(ruleNode);
				for(P premise : body) {
					final OrNode<P> premiseNode = getNode(premise, propositionToNode);
					ruleNode.addChild(premiseNode);
					premiseNode.addParent(ruleNode);
				}
			}
		}
		return propositionToNode;
	}
	
	private final static <P> OrNode<P> getNode(final P proposition, final Map<P, OrNode<P>> map){
		return map.computeIfAbsent(proposition, p -> new OrNode<>(p));
	}
}


