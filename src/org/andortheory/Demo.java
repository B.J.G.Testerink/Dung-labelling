package org.andortheory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;

public final class Demo {

	public final static void main(final String[] args) {
		demo3(); 
	}
	
	private final static void demo1() {
		// Make theory
		final Map<String, Set<Set<String>>> rules = parseRules(
				s -> s,
				"=>",
				  "A,B=>D;"
				+ "A,C=>D;"
				+ "A,C=>F;"
				+ "D=>E;"
				+ "E=>C;");
		
		final Set<String> observations = new HashSet<>();
		observations.add("A");
		observations.add("B");  
		observations.add("C");  

		outputDungGraph(AndOrGraph.produceDungGraph(AndOrGraph.buildAndOrGraph(rules), observations, Demo::contrary));
		
		// Entry<Argument, Map<Argument, Set<Argument>>> dungGraph = AndOrGraph.produceDungGraph(rules, observations, new String[0]);
		
		// Get labelings
		
		// Output conclusions
	}

	public final static void demo2() {
		// Make theory
		final Map<String, Set<Set<String>>> rules = parseRules(
				s -> s,
				"=>",
				  "A=>B;"
				+ "B=>C;"
				+ "C=>A;"
				+ "C=>B;"
				+ "A=>C;"
				+ "B=>A;"
				+ "B=>D;");
		
		final Set<String> observations = new HashSet<>();
		observations.add("A");  

		outputDungGraph(AndOrGraph.produceDungGraph(AndOrGraph.buildAndOrGraph(rules), observations, Demo::contrary));
	}
	
	public final static void demo3() {
		// Make theory
		final Map<String, Set<Set<String>>> rules = parseRules(
				s -> s,
				"=>",
				  "A=>C;"
				+ "B=>D;"
				+ "D=>notF;"
				+ "A,C=>F;"
				+ "F=>G;");
		
		final Set<String> observations = new HashSet<>();
		observations.add("A");  
		observations.add("B");
		
		outputDungGraph(AndOrGraph.produceDungGraph(AndOrGraph.buildAndOrGraph(rules), observations, Demo::contrary));
	} 
	
	public final static void outputDungGraph(final Entry<Argument<String>[], Function<Argument<String>, Set<Argument<String>>>> dungGraph) {
		
		// TEST
		for(Argument<String> argument : dungGraph.getKey()) { 
				System.out.println(argument);
		}
	}
	
	public final static boolean contrary(final String a, final String b) {
		//System.out.println(a +" "+b+" "+(negates(a, b) || negates(b, a))+" "+a.startsWith("not") +" "+ (a.length() == b.length()+3) +" "+ a.regionMatches(false, 3, b, 0, b.length()));
		return negates(a, b) || negates(b, a);
	}
	
	private final static boolean negates(final String a, final String b) {
		return a.startsWith("not") && (a.length() == b.length()+3) && a.regionMatches(false, 3, b, 0, b.length());
	}
	
	public final static <P> Map<P, Set<Set<P>>>  parseRules(final Function<String, P> parser, final String ruleDelimiter, final String input) {
		final Map<P, Set<Set<P>>> rules = new HashMap<>();
		final String[] rulesAsStrings = input.replaceAll("\\s","").split(";");
		for(String ruleAsString : rulesAsStrings) {
			final String[] decomposition = ruleAsString.split(",|"+ruleDelimiter);
			final int indexHead = decomposition.length - 1;
			final List<P>  body = new ArrayList<>();
			for(int i = 0; i < indexHead; i++)
				body.add(parser.apply(decomposition[i]));
			rules
				.computeIfAbsent(parser.apply(decomposition[indexHead]), p -> new HashSet<>())
				.add(new HashSet<>(body));
		}
		return rules;
	}
}
