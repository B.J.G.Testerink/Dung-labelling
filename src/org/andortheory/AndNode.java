package org.andortheory;

import java.util.ArrayList;
import java.util.List;

final class AndNode<T> {
	private final OrNode<T> parent;
	private final List<OrNode<T>> children;
	private boolean active;
	
	AndNode(final OrNode<T> parent) {
		this.parent = parent;
		this.children = new ArrayList<>();
		this.active = false;
	}
	
	final void activate() {
		if(this.active) return;
		for(OrNode<?> child : children)
			if(!child.isActive())
				return;
		this.active = true;
		parent.activate();
	}
	
	final boolean isActive() {
		return this.active;
	} 
	
	final void addChild(final OrNode<T> child) {
		this.children.add(child);
	} 

	final OrNode<T> getParent(){
		return this.parent;
	}

	final List<OrNode<T>> getChildren(){
		return this.children;
	}
}
