package org.andortheory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Argument<P> {
	private final P conclusion;
	private final List<Argument<P>> premises;
	private final Set<P> propositionDependencies; // All the (intermediate) premises
	
	public Argument(final P conclusion) {
		this.conclusion = conclusion;
		this.premises = new ArrayList<>();
		this.propositionDependencies = new HashSet<>();
		this.propositionDependencies.add(conclusion);
	}
	
	public final boolean dependsOn(final P premise) {
		return this.propositionDependencies.contains(premise);
	}
	
	public final void addPremise(final Argument<P> argument) {
		this.premises.add(argument);
		this.propositionDependencies.addAll(argument.getPropositionDependencies());
	}
	
	public final List<Argument<P>> getPremises(){
		return this.premises;
	}
	
	public final P getConclusion() {
		return this.conclusion;
	}
	
	final Set<P> getPropositionDependencies() {
		return this.propositionDependencies;
	}
	
	public final String toString() {
		if(premises.size() == 0) return (String) this.conclusion;
		final StringBuffer buffer = new StringBuffer();
		buffer.append(conclusion).append(":[");
		for(Argument<P> premise : this.premises)
			buffer.append(" ").append(premise);
		buffer.append(" ]");
		return buffer.toString();
	}
}
