package org.andortheory;

import java.util.ArrayList;
import java.util.List;

public final class OrNode<P> {
	private final List<AndNode<P>> parents;
	private final List<AndNode<P>> children;
	private final P content;
	private boolean active, observed;
	
	OrNode(final P content) {
		this.parents = new ArrayList<>();
		this.children = new ArrayList<>();
		this.content = content;
		this.active = false;
		this.observed = false;
	}
	
	final void observed() {
		this.observed = true;
		activate();
	}
	
	final void activate() {
		if(this.active) return;
		this.active = true; 
		for(AndNode<P> parent : parents)
			parent.activate();
	}
	
	final boolean isActive() {
		return this.active;
	}
	
	final boolean isObserved() {
		return this.observed;
	}
	
	final void addParent(final AndNode<P> parent) {
		this.parents.add(parent);
	}
	
	final void addChild(final AndNode<P> child) {
		this.children.add(child);
	}
	
	final P getContent() {
		return this.content;
	}

	final List<AndNode<P>> getParents(){
		return this.parents;
	}

	final List<AndNode<P>> getChildren(){
		return this.children;
	}
	
}
