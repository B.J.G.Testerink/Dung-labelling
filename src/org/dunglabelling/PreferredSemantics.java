package org.dunglabelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class PreferredSemantics {

	public final static <A> Collection<Map<A, Label>> labelStartWithGrounded(final A[] arguments, final Function<A, Set<A>> attackRelation){
		final SearchState<A> search = new SearchState<>(arguments, attackRelation, true);
		label(search);
		return search.getExtensions();
	} 
	
	public final static <A> Collection<Map<A, Label>> label(final A[] arguments, final Function<A, Set<A>> attackRelation){
		final SearchState<A> search = new SearchState<>(arguments, attackRelation, false);
		label(search);
		return search.getExtensions();
	} 
	
	private final static <A> void label(final SearchState<A> search) {
		// Check if it is a preferred extension if all arguments have a label
		if(search.done())
			search.storeLabelingIfPossiblyPreferredExtension();
		else {
			final A blank = search.getBlankArgument(); 
			
			// Remember the labels that you might overwrite
			if(!search.getAttackers(blank).contains(blank)) {
				final Map<A, Label> overwritten = new HashMap<>();
				for(A attacker : search.getAttackers(blank))
					overwritten.put(attacker, search.getLabel(attacker));
				for(A attacked : search.getAttackedArguments(blank))
					overwritten.put(attacked, search.getLabel(attacked));
				
				// Label the blank IN
				search.setLabel(blank, Label.IN);
				// Attacked arguments are now OUT, and the attackers must be out
				for(A attacked : search.getAttackedArguments(blank)) 
					search.label(attacked, Label.OUT);  
				for(A attacker : search.getAttackers(blank))
					search.label(attacker, Label.MUST_OUT);
				
				// Continue the search 
				label(search); 
				
				// Undo the IN-transition labeling
				for(Entry<A, Label> pair : overwritten.entrySet())
					search.undoLabel(pair.getKey(), pair.getValue());
			}
			
			
			// Set the blank to undecided and continue 
			search.setLabel(blank, Label.UNDECIDED); 
			label(search);
			
			// Undo the label and continue with parent call
			search.undoLabel(blank, null); 
		}
	}
	
	private final static class SearchState<A> {
		private final A[] arguments;
		private final Function<A, Set<A>> attackRelation;
		private final Map<A, Set<A>> attackRelationReversed;
		private final Map<A, Label> labeling; 
		private final Collection<Map<A, Label>> labelings;
		private final Collection<A> blanks;
		private int nrMustOut;
		
		SearchState(final A[] arguments, final Function<A, Set<A>> attackRelation, boolean startWithGrounded){
			this.arguments = arguments; 
			if(startWithGrounded) {
				this.labeling = GroundedSemantics.label(arguments, attackRelation);
				this.blanks = new HashSet<>();
				for(A argument : arguments) {
					final Label label = this.labeling.get(argument);
					if(label == Label.UNDECIDED) {
						this.labeling.remove(argument);
						this.blanks.add(argument); 
					}
				}
			} else {
				this.labeling = new HashMap<>();
				this.blanks = new HashSet<>(Arrays.asList(arguments));
			}
			
			this.attackRelation = attackRelation;
			this.attackRelationReversed = GroundedSemantics.reverseAttackRelation(arguments, attackRelation);
			
			this.labelings = new HashSet<>();
			this.nrMustOut = 0;
		}
		
		final void setLabel(final A argument, final Label label) {
			this.labeling.put(argument, label); 
		}
		
		final Label getLabel(final A argument) {
			return this.labeling.get(argument);
		}

		final Set<A> getAttackers(final A argument){
			return this.attackRelationReversed.getOrDefault(argument, Collections.emptySet());
		}
		
		final Set<A> getAttackedArguments(final A argument){
			final Set<A> defended = this.attackRelation.apply(argument);
			return defended == null ? Collections.emptySet() : defended; 
		}
		
		final boolean done() {
			return this.blanks.isEmpty();
		}
		
		final A getBlankArgument() {
			final Iterator<A> iterator = this.blanks.iterator();
			final A blank = iterator.next();
			iterator.remove();
			return blank;
		}
		
		final void label(final A argument, final Label label) {
			final Label previous = this.labeling.get(argument);
			if(previous == null)
				blanks.remove(argument); 
			if(label == Label.MUST_OUT && (previous == null || previous == Label.UNDECIDED)) {
				this.nrMustOut++;
				this.labeling.put(argument, label);
			} else if(label == Label.OUT && previous == Label.MUST_OUT) {
				this.nrMustOut--;
				this.labeling.put(argument, label);
			} else if(previous == null) {
				this.labeling.put(argument, label); 
			} else if(previous == Label.UNDECIDED) {
				this.labeling.put(argument, label);
			}
		}
		
		final void undoLabel(final A argument, final Label label) {
			final Label previous = this.labeling.get(argument);
			if(previous != label) {
				if(previous == Label.MUST_OUT) {
					this.nrMustOut--;
				} else if(label == Label.MUST_OUT) {
					this.nrMustOut++; 
				}
				this.labeling.put(argument, label);
				if(label == null) {
					this.blanks.add(argument); 
				}
			}
		}
		
		final void storeLabelingIfPossiblyPreferredExtension() {
			// A MUSTOUT is a label that should always be later-on verified to 
			// be OUT. Otherwise, it is 'unnecessarily' labeled out
			if(this.nrMustOut > 0) return;
			
			final Collection<Map<A, Label>> toRemove = new ArrayList<>();
			boolean encapsulated = false;
			for(Map<A, Label> oldLabeling : this.labelings) { 
				Set<A> oldIn = oldLabeling.keySet().stream().filter(k -> oldLabeling.get(k) == Label.IN).collect(Collectors.toSet());
				Set<A> newIn = this.labeling.keySet().stream().filter(k -> this.labeling.get(k) == Label.IN).collect(Collectors.toSet());
				if(newIn.containsAll(oldIn))
					toRemove.add(oldLabeling);
				else if(oldIn.containsAll(newIn)) {
					// Note that if the new labeling is already contained, then there is no labeling
					// which is encapsulated by the new labeling (otherwise that labeling would have 
					// been encapsulated as well by the labeling that encapsulates the new labeling).
					return;
				} 
			}
			this.labelings.removeAll(toRemove);
			// Store a copy of the labeling
			labelings.add(new HashMap<>(this.labeling));
		} 
		
		final Collection<Map<A, Label>> getExtensions(){
			return this.labelings;
		}
	}
}
