package org.dunglabelling;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.andortheory.AndOrGraph;
import org.andortheory.Argument;
import org.andortheory.OrNode; 

public final class Demo {
	public final static void main(final String[] commandLineArguments) {
//		System.out.println("Demo 1");
//		demo1();
//		System.out.println();
//		System.out.println("Demo 2");
//		demo2();
		
//		demo3();
//		demo4();
		demoTheoryToLabeling();
//		demo5();
//		demo6();
	}
	
	private final static void demoTheoryToLabeling() {
		// Make theory
		final Map<String, Set<Set<String>>> rules = org.andortheory.Demo.parseRules(
				s -> s,
				"=>",
				  "A,B=>D;"
				+ "A,C=>D;"
				+ "A,C=>F;"
				+ "D=>E;"
				+ "E=>C;");
		
		final Set<String> observations = new HashSet<>();
		observations.add("A");
		observations.add("B");  
		observations.add("C");  
		
		// Make the and-or graph
		final Map<String, OrNode<String>> andOrGraph = AndOrGraph.buildAndOrGraph(rules);
		
		// Make the Dung-graph
		final Entry<Argument<String>[], Function<Argument<String>, Set<Argument<String>>>> dungGraph = 
				AndOrGraph.produceDungGraph(
						andOrGraph, 
						observations, 
						org.andortheory.Demo::contrary); 
		
		// Make the attack map
		final Map<Argument<String>, Set<Argument<String>>> attackRelation = new HashMap<>();
		for(Argument<String> argument : dungGraph.getKey())
			attackRelation.put(argument, dungGraph.getValue().apply(argument));
		
		// Make and show the labeling
		showGroundedLabeling(dungGraph.getKey(), attackRelation);  
	}

	private final static void demo1() {
		// Make Dung graph (arguments are Strings in this demo)
		final String[] arguments = new String[] { "A", "B", "C", "D", "E", "F" };
		final Map<String, Set<String>> attackRelation = new HashMap<>();
		addEdge(arguments[0], arguments[1], attackRelation); // A -> B
		addEdge(arguments[1], arguments[2], attackRelation); // B -> C
		addEdge(arguments[1], arguments[3], attackRelation); // B -> D
		addEdge(arguments[3], arguments[1], attackRelation); // D -> B
		addEdge(arguments[3], arguments[4], attackRelation); // D -> E
		addEdge(arguments[4], arguments[3], attackRelation); // E -> D
		addEdge(arguments[3], arguments[5], attackRelation); // D -> F
		addEdge(arguments[4], arguments[5], attackRelation); // E -> F
		
		showGroundedLabeling(arguments, attackRelation);
		showPreferredLabelings(arguments, attackRelation);
	}
	
	private final static void demo2() {
		// Make Dung graph (arguments are Strings in this demo)
		final String[] arguments = new String[] { "A", "B", "C", "D", "E", "F" };
		final Map<String, Set<String>> attackRelation = new HashMap<>();
		addEdge(arguments[0], arguments[1], attackRelation); // A -> B
		addEdge(arguments[1], arguments[0], attackRelation); // B -> A
		addEdge(arguments[0], arguments[2], attackRelation); // A -> C
		addEdge(arguments[1], arguments[2], attackRelation); // B -> C
		addEdge(arguments[2], arguments[3], attackRelation); // C -> D
		
		showGroundedLabeling(arguments, attackRelation);
		showPreferredLabelings(arguments, attackRelation);
	}
	
	private final static void demo3() {
		// This demonstrates a random graph with 25 arguments
		final Character[] arguments = new Character[25];
		for(int i = 0; i < arguments.length; i++)
			arguments[i] = (char) (65 + i);
		final Random random = new Random(1);
		
		// p*100 = percentage of change that two arbitrary arguments A,B are connected as A -> B
		final double p = 0.1155;
		final Map<Character, Set<Character>> attackRelation = randomAttackRelation(arguments, random, p);
		
		showGroundedLabeling(arguments, attackRelation);
		
	}
	
	private final static void demo4() {
		final Integer[] arguments = new Integer[25];
		for(int i = 0; i < arguments.length; i++)
			arguments[i] = i;
		final Random random = new Random(1);
		final ExecutorService executor = Executors.newFixedThreadPool(8);
		final boolean concurrent = false;
		
		final long start = System.currentTimeMillis();
		for(double p = 0.01; p < 1.01; p += 0.01) {
			final long startP = System.currentTimeMillis();
			int nrOfExtensions = 0;
			if(concurrent) {
				final List<Callable<Integer>> tasks = new ArrayList<>();
				for(int i = 0; i < 10; i++) {
					final Map<Integer, Set<Integer>> attackRelation = randomAttackRelation(arguments, random, p);
					tasks.add(()-> {
						return PreferredSemantics.labelStartWithGrounded(arguments, a -> attackRelation.get(a)).size();
					}); 
				}
				try { 
					for(Future<Integer> call : executor.invokeAll(tasks))
						nrOfExtensions += call.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			} else {
				for(int i = 0; i < 10; i++) {
					final Map<Integer, Set<Integer>> attackRelation = randomAttackRelation(arguments, random, p);
					nrOfExtensions += PreferredSemantics.labelStartWithGrounded(arguments, a -> attackRelation.get(a)).size();
				}
			}
			
			System.out.println("p = " +p+" avg. duration 10 instances: "+((System.currentTimeMillis()-startP))*0.1 + "ms, nr of extensions ="+nrOfExtensions);
		} 
		System.out.println("Done " + (System.currentTimeMillis() - start) +" ms duration");
	}
	
	private final static void demo5() {
		// Make Dung graph (arguments are Strings in this demo)
		final String[] arguments = new String[] { "A", "B", "C"};
		final Map<String, Set<String>> attackRelation = new HashMap<>();
		addEdge(arguments[0], arguments[1], attackRelation); // A -> B
		addEdge(arguments[1], arguments[2], attackRelation); // B -> A
		addEdge(arguments[2], arguments[0], attackRelation); // A -> C 
		
		showGroundedLabeling(arguments, attackRelation);
		showPreferredLabelings(arguments, attackRelation);
	}
	
	private final static <A> void showGroundedLabeling(final A[] arguments, final Map<A, Set<A>> attackRelation) {
		// Make and show the labeling
		System.out.println("Grounded: ");
		final Map<A, Label> labeling = GroundedSemantics.label(arguments, a -> attackRelation.get(a));
		for(A argument : arguments)
			System.out.println(argument + " is labeled " + labeling.get(argument));
	}
	
	private final static <A> void showPreferredLabelings(final A[] arguments, final Map<A, Set<A>> attackRelation) {
		final Collection<Map<A, Label>> labelings = PreferredSemantics.label(arguments, a -> attackRelation.get(a));
		System.out.println("Preferred: ");
		int i = 0;
		for(Map<A, Label> extension : labelings) {
			i++;
			System.out.println("Extension "+i);
			for(A argument : arguments)
				System.out.println(argument + " is labeled " + extension.get(argument));
		}
	}
	private final static void demo6() {
		// Make Dung graph (arguments are Strings in this demo)
		final String[] arguments = new String[] { "A", "B", "C", "D", "E" };
		final Map<String, Set<String>> attackRelation = new HashMap<>();
		addEdge(arguments[0], arguments[1], attackRelation); // A -> B
		addEdge(arguments[0], arguments[2], attackRelation); // A -> C
		addEdge(arguments[0], arguments[3], attackRelation); // A -> D
		addEdge(arguments[0], arguments[4], attackRelation); // A -> E
		addEdge(arguments[1], arguments[0], attackRelation); // B -> A
		addEdge(arguments[2], arguments[0], attackRelation); // C -> A 
		
		showGroundedLabeling(arguments, attackRelation);
		showPreferredLabelings(arguments, attackRelation);
	}
	
	/** Add the attacked arguments to the set of arguments that the attacker attacks. */
	private final static <A> void addEdge(final A attacker, final A attacked, final Map<A, Set<A>> attackRelation) {
		attackRelation
			.computeIfAbsent(attacker, a -> new HashSet<>())
			.add(attacked);
	}
	
	private final static <A> Map<A, Set<A>> randomAttackRelation(final A[] arguments, final Random random, final double p){
		final Map<A, Set<A>> attackRelation = new HashMap<>();
		for(A attacker : arguments)
			for(A attacked : arguments)
				if(random.nextDouble() <= p && attacked != attacker)
					addEdge(attacker, attacked, attackRelation);
		return attackRelation;
	}
}
