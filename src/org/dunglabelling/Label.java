package org.dunglabelling;

public enum Label {
	IN, OUT, UNDECIDED, MUST_OUT, BLANK;
}
