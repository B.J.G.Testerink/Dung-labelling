package org.dunglabelling;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;

public final class GroundedSemantics {
	
	public final static <A> Map<A, Label> label(final A[] arguments, final Function<A, Set<A>> attackRelation) {
		// Get the reverse of the attack relation in order to determine when all 
		// attackers of an argument are labelled out
		final Map<A, Set<A>> incomingAttacks = reverseAttackRelation(arguments, attackRelation); 
		
		// The main queue of arguments that we know for certain to be IN
		final Queue<A> inArguments = new LinkedList<>();
		for(A argument : arguments)
			if(incomingAttacks.get(argument).isEmpty())
				inArguments.add(argument);
		
		// Create the actual labeling
		final Map<A, Label> labeling = new HashMap<>();
		while(!inArguments.isEmpty()) {
			final A argument = inArguments.poll();
			labeling.put(argument, Label.IN);
			
			// Handle the ramifications of labeling the argument IN
			final Set<A> attackedArguments = attackRelation.apply(argument);
			if(attackedArguments != null) {
				for(A attacked : attackedArguments) {
					if(labeling.get(attacked) == null) {
						// The attacked argument must be out because it has an IN attacker
						labeling.put(attacked, Label.OUT);
						// Now there will be arguments defended by the IN argument
						final Set<A> defendedArguments = attackRelation.apply(attacked);
						if(defendedArguments != null) {
							for(A defended : defendedArguments) {
								final Set<A> attackers = incomingAttacks.get(defended);
								attackers.remove(attacked); 
								// If all attackers are out of the defended argument, then it 
								// must be IN (and the defended argument now may defend others)
								if(attackers.isEmpty())
									inArguments.add(defended); 
							}
						}
					}
				} 
			}
		}
		
		// All arguments without a label must be UNDECIDED
		for(A argument : arguments)
			labeling.computeIfAbsent(argument, a -> Label.UNDECIDED);
		
		return labeling;
	}
	
	/** Returns the reverse of the attack relation (a map that given an argument returns the set of the arguments 
	 * that attack it) */
	final static <A> Map<A, Set<A>> reverseAttackRelation(final A[] arguments, final Function<A, Set<A>> attackRelation){
		// Obtain the incoming attacks
		final Map<A, Set<A>> incomingAttacks = new HashMap<>();
		for(A attacker : arguments) {
			incomingAttacks.computeIfAbsent(attacker, a -> new HashSet<>());
			final Set<A> attackedArguments = attackRelation.apply(attacker);
			if(attackedArguments != null) {
				for(A attacked : attackedArguments) {
					incomingAttacks
						.computeIfAbsent(attacked, a -> new HashSet<>())
						.add(attacker);
				}
			}
		}
		return incomingAttacks;
	}
}
